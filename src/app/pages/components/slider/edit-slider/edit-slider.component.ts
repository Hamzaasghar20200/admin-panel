import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from "ngx-toastr";
import { SliderService } from 'src/app/service/slider.service';
import { environment } from './../../../../../environments/environment';

@Component({
  selector: 'app-edit-slider',
  templateUrl: './edit-slider.component.html',
  styleUrls: ['./edit-slider.component.scss']
})
export class EditSliderComponent implements OnInit {

  base_image_url: string = environment.image_url;
  loadSlide: any;
  selectedFile: File = null;
  slideID: any;

  constructor(
    private activatedRoute: ActivatedRoute,
    private sliderService: SliderService,
    private toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.slideID = this.activatedRoute.snapshot.paramMap.get('id');
    this.editSlider();
  }



  editSlider() {

    this.sliderService.editSlider({ id: this.slideID }).subscribe(
      (data: any) => {
        console.log(data)
        if (data.code === 200) {
          this.loadSlide = this.base_image_url+data.data.banner_file
        }
      },
      (error) => {
        this.toastr.error(error, "Error");
      }
    );
  }



  // FOR IMAGE PREWVEW
  onSelectFile(e){

    this.selectedFile = e.target.files[0]

    if(e.target.files){
      var reader = new FileReader();
      reader.readAsDataURL(this.selectedFile)
      reader.onload=(event: any) => {
        this.loadSlide = event.target.result;
      }
    }

  }


  onUpdateUpload(){
    const fd = new FormData();
    fd.append('id', this.slideID);
    fd.append('banner_image', this.selectedFile, this.selectedFile.name);
    this.sliderService.updateSlider(fd)
    .subscribe(
      (res) => {
        if(res.code === 200){
          this.toastr.success("Slider Image Update Successfully", "Success");
          this.router.navigate(['/slider']);
        }
        console.log(res)
      },
      (err) => console.log(err)
    );
  }

}
