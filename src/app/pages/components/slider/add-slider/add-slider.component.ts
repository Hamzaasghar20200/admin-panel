import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder} from '@angular/forms';
import { ToastrService } from "ngx-toastr";
import { SliderService } from 'src/app/service/slider.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-slider',
  templateUrl: './add-slider.component.html',
  styleUrls: ['./add-slider.component.scss']
})
export class AddSliderComponent implements OnInit {

  sliderForm: FormGroup;
  url = '';
  selectedFile: File = null;

  constructor(
    private sliderService: SliderService,
    private toastr: ToastrService,
    private formBuilder: FormBuilder,
    private router: Router
  ) { }


  ngOnInit(): void {

    this.sliderForm = this.formBuilder.group({
      banner_image: [null],
    });

  }

  // FOR IMAGE PREWVEW
  onSelectFile(e){

    this.selectedFile = e.target.files[0]

    if(e.target.files){
      var reader = new FileReader();
      reader.readAsDataURL(this.selectedFile)
      reader.onload=(event: any) => {
        this.url = event.target.result;
      }
    }

  }

  onUplaod(){
    const fd = new FormData();
    fd.append('banner_image', this.selectedFile, this.selectedFile.name);
    this.sliderService.addSlider(fd)
    .subscribe(
      (res) => {
        if(res.code === 200){
          this.toastr.success("Slider Image Upload Successfullt", "Success");
          this.router.navigate(['/slider']);
        }
        console.log(res)
      },
      (err) => console.log(err)
    );
  }


  // onSelectFile(e){


  //   const file = (event.target as HTMLInputElement).files[0];
  //   console.log(file);
  //   this.sliderForm.patchValue({
  //     banner_image: file
  //   });
  //   this.sliderForm.get('banner_image').updateValueAndValidity();

  //   this.selectedFile = e.target.files[0]
  //   if(e.target.files){
  //     var reader = new FileReader();
  //     reader.readAsDataURL(this.selectedFile)
  //     reader.onload=(event: any) => {
  //       this.url = event.target.result;
  //     }
  //   }

  // }


  onSubmit() {
    debugger
    const fd = new FormData();
    fd.append('banner_image',  this.sliderForm.value.banner_image);
    console.log(fd)
    this.sliderService.addSlider(fd)
    .subscribe(
      (res) => {
        if(res.code === 200){
          this.toastr.success("Slider Image Upload Successfullt", "Success");
        }
        console.log(res)
      },
      (err) => console.log(err)
    );
  }


}
