import { Component, OnInit } from '@angular/core';
import { ToastrService } from "ngx-toastr";
import { SliderService } from 'src/app/service/slider.service';
import { environment } from './../../../../environments/environment';
import { remove } from 'lodash';


@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.scss']
})
export class SliderComponent implements OnInit {

  base_image_url: string = environment.image_url;
  loadSlideData: any;

  constructor(
    private sliderService: SliderService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.getAllSliders();
  }

  getAllSliders() {
    this.sliderService.getSlider({}).subscribe((data: any) => {
      if (data.code === 200) {
        console.log(data)
        this.loadSlideData = data.data;
      }
    });
  }


  deleteSlider(slider_id: any) {

    this.sliderService.deleteSlider({ id: slider_id }).subscribe(
      (data: any) => {
        if (data.code === 200) {
          this.toastr.success("User Deleted Successfully", "Success");

          remove(this.loadSlideData, (slide: any) => {
            return slide.id === slider_id
          });
          this.loadSlideData = [...this.loadSlideData];

        }
      },
      (error) => {
        this.toastr.error(error, "Error",);
      }
    );
  }


}
