import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlideAddCategoryComponent } from './slide-add-category.component';

describe('SlideAddCategoryComponent', () => {
  let component: SlideAddCategoryComponent;
  let fixture: ComponentFixture<SlideAddCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlideAddCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlideAddCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
