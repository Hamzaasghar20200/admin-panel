import { SlideCategoryService } from './../../../../service/slide-category.service';
import { ServiceService } from "src/app/service/service.service";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: 'app-slide-add-category',
  templateUrl: './slide-add-category.component.html',
  styleUrls: ['./slide-add-category.component.scss']
})
export class SlideAddCategoryComponent implements OnInit {

  sideCategoryForm: FormGroup;
  loadServiceData: any = [];
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private slideCategoryService: SlideCategoryService,
    private serviceService: ServiceService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.form();
    this.getAllServices();
  }


  getAllServices() {
    this.serviceService.getServices().subscribe((data: any) => {
      if (data.code === 200) {
        this.loadServiceData = data.data;
        console.log(this.loadServiceData)

      }
    });
  }


  selectService(e) {
    var val = e.target.value.split(':')[1];
    this.sideCategoryForm.get("service_id").setValue(val);
  }

  form() {
    this.sideCategoryForm = this.formBuilder.group({
      title: ["", [Validators.required]],
      service_id: ["", Validators.required],
    });
  }

  get f() {
    return this.sideCategoryForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.sideCategoryForm.invalid) {
      return;
    }

    const fd = new FormData();
    fd.append("title", this.sideCategoryForm.value.title);
    fd.append("service_id", this.sideCategoryForm.value.service_id);

    this.slideCategoryService.addSlideCategory(fd).subscribe((data: any) => {
      console.log(data);
      if (data.code === 200) {
        this.toastr.success("Slide category Added Successfully", "Success");
        this.sideCategoryForm.reset();
        this.submitted = false;
      } else {
        this.toastr.error(data.message, "Error");
      }
    });
  }

}
