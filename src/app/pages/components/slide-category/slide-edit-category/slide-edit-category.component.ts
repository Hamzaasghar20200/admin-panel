import { SlideCategoryService } from './../../../../service/slide-category.service';
import { ServiceService } from "src/app/service/service.service";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-slide-edit-category',
  templateUrl: './slide-edit-category.component.html',
  styleUrls: ['./slide-edit-category.component.scss']
})
export class SlideEditCategoryComponent implements OnInit {
  slideCategoryUpdatedForm: FormGroup;
  submitted = false;
  loadSlideCategory: any;
  loadServiceData: any = [];
  slideCategoryID: any;

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private slideCategoryService: SlideCategoryService,
    private serviceService: ServiceService,
    private toastr: ToastrService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.form();
    this.slideCategoryID = this.activatedRoute.snapshot.paramMap.get("id");
    this.getAllServices();
    this.editSlideCaegory();
  }

  form() {
    this.slideCategoryUpdatedForm = this.formBuilder.group({
      title: ["", [Validators.required]],
      service_id: ["", Validators.required],
    });
  }

  getAllServices() {
    this.serviceService.getServices().subscribe((data: any) => {
      if (data.code === 200) {
        this.loadServiceData = data.data;
        console.log(this.loadServiceData);
      }
    });
  }


  selectService(e) {
    var val = e.target.value.split(':')[1];
    this.slideCategoryUpdatedForm.get("service_id").setValue(val);
  }

  editSlideCaegory() {
    this.slideCategoryService.editSlideCategory({ id: this.slideCategoryID }).subscribe(
      (data: any) => {
        console.log(data);
        if (data.code === 200) {
          this.loadSlideCategory = data.data;
          console.log(this.loadSlideCategory);
        }
      },
      (error) => {
        this.toastr.error(error, "Error");
      }
    );
  }

  get f() {
    return this.slideCategoryUpdatedForm.controls;
  }


  onSubmit() {
    this.submitted = true;

    if (this.slideCategoryUpdatedForm.invalid) {
      return;
    }

    const fd = new FormData();
    fd.append("id", this.slideCategoryID);
    fd.append("title", this.slideCategoryUpdatedForm.value.title);
    fd.append("service_id", this.slideCategoryUpdatedForm.value.service_id);

    this.slideCategoryService.updateSlideCategory(fd).subscribe((data: any) => {
      console.log(data);
      if (data.code === 200) {
        this.toastr.success("SlideCategoy Updated Successfully", "Success");
        this.submitted = false;
        this.router.navigate(["/category-slide"]);
      } else {
        this.toastr.error(data.message, "Error");
      }
    });
  }
}
