import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlideEditCategoryComponent } from './slide-edit-category.component';

describe('SlideEditCategoryComponent', () => {
  let component: SlideEditCategoryComponent;
  let fixture: ComponentFixture<SlideEditCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlideEditCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlideEditCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
