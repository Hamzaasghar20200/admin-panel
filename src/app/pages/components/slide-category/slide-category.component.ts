import { SlideCategoryService } from './../../../service/slide-category.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from "ngx-toastr";
import { environment } from './../../../../environments/environment';
import { remove } from 'lodash';

@Component({
  selector: 'app-slide-category',
  templateUrl: './slide-category.component.html',
  styleUrls: ['./slide-category.component.scss']
})
export class SlideCategoryComponent implements OnInit {

  loadSlideCategoryData: any;

  constructor(
    private slideCategoryService: SlideCategoryService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.getAllSlideCategory();
  }


  getAllSlideCategory() {
    this.slideCategoryService.getSlideCategory().subscribe((data: any) => {
      if (data.code === 200) {
        console.log(data)
        this.loadSlideCategoryData = data.data;
      }
    });
  }


  deleteSlidecategory(slideCategory_id: any) {

    this.slideCategoryService.deleteSlideCategory({ id: slideCategory_id }).subscribe(
      (data: any) => {
        if (data.code === 200) {
          this.toastr.success("Slide Category Deleted Successfully", "Success");

          remove(this.loadSlideCategoryData, (slideCategory: any) => {
            return slideCategory.id === slideCategory_id
          });
          this.loadSlideCategoryData = [...this.loadSlideCategoryData];

        }
      },
      (error) => {
        this.toastr.error(error, "Error",);
      }
    );
  }


}
