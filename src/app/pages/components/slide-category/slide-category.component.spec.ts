import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SlideCategoryComponent } from './slide-category.component';

describe('SlideCategoryComponent', () => {
  let component: SlideCategoryComponent;
  let fixture: ComponentFixture<SlideCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SlideCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SlideCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
