import { UserService } from "./../../../service/user.service";
import { Component, OnInit } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import $ from "jquery";
@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.scss"],
})
export class UsersComponent implements OnInit {
  loadUserData: any;

  constructor(
    private userService: UserService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.getAllUser();

    // SEARCH USE
    $(document).ready(function () {
      $("#myInput").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#myTable tr").filter(function () {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
        });
      });
    });
  }

  getAllUser() {
    this.userService.listOfUsers().subscribe((data: any) => {
      if (data.code === 200) {
        this.loadUserData = data.data;
      }
    });
  }

  blockUnbockUser(user_id: any, status: any) {
    if (status === "1") {
      // blockapi
      this.userService.blockUser({ user_id: user_id }).subscribe(
        (data: any) => {
          if (data.code === 200) {
            this.toastr.info("User Is Block", "MSg", {
              positionClass: "toast-top-center",
            });
            this.getAllUser();
          }
        },
        (error) => {
          this.toastr.error(error, "", {
            positionClass: "toast-top-center",
          });
        }
      );
    } else if (status === "2") {
      // unblockapicode
      this.userService.unBlockUser({ user_id: user_id }).subscribe(
        (data: any) => {
          if (data.code === 200) {
            this.toastr.info("User Is Un Block", "Msg", {
              positionClass: "toast-top-center",
            });
            this.getAllUser();
          }
        },
        (error) => {
          this.toastr.error(error, "", {
            positionClass: "toast-top-center",
          });
        }
      );
    }
  }

  deleteUser(user_id: any) {

      this.userService.delete_user({ user_id: user_id }).subscribe(
        (data: any) => {
          if (data.code === 200) {
            this.toastr.success("User Deleted Successfully", "Success");
            this.getAllUser();
          }
        },
        (error) => {
          this.toastr.error(error, "Error",);
        }
      );

  }
}
