import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { PakagesService } from "src/app/service/pakages.service";
import { ServiceService } from "src/app/service/service.service";
import { environment } from "./../../../../../environments/environment";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-edit-package',
  templateUrl: './edit-package.component.html',
  styleUrls: ['./edit-package.component.scss']
})
export class EditPackageComponent implements OnInit {

  base_image_url: string = environment.image_url;
  pakagesUpdatedForm: FormGroup;
  submitted = false;
  loadServiceData: any = [];
  loadPakagesData: any;
  selectedFile: File = null;
  pakagesID: any;
  loadPakagesImage: any;

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private pakagesService: PakagesService,
    private serviceService: ServiceService,
    private toastr: ToastrService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.form();
    this.pakagesID = this.activatedRoute.snapshot.paramMap.get("id");
    this.editPakages();
    this.getAllServices();
  }

  form() {
    this.pakagesUpdatedForm = this.formBuilder.group({
      title: ["", [Validators.required]],
      price: ["", [Validators.required]],
      service_id: ["", Validators.required],
      start_date: ["", Validators.required],
      end_date: ["", Validators.required],
      image: [""],
    });
  }

  getAllServices() {
    this.serviceService.getServices().subscribe((data: any) => {
      if (data.code === 200) {
        this.loadServiceData = data.data;
        console.log(this.loadServiceData);
      }
    });
  }

  editPakages() {
    this.pakagesService
      .editPackages({ id: this.pakagesID })
      .subscribe(
        (data: any) => {
          console.log(data);
          if (data.code === 200) {
            this.loadPakagesData = data.data;
            this.loadPakagesImage =
              this.base_image_url + this.loadPakagesData.image;
            console.log(this.loadPakagesData);
          }
        },
        (error) => {
          this.toastr.error(error, "Error");
        }
      );
  }

  // FOR IMAGE PREWVEW
  onSelectFile(e) {
    this.selectedFile = e.target.files[0];
    this.pakagesUpdatedForm.get("image").setValue(this.selectedFile);

    if (e.target.files) {
      var reader = new FileReader();
      reader.readAsDataURL(this.selectedFile);
      reader.onload = (event: any) => {
        this.loadPakagesImage = event.target.result;
      };
    }
  }

  // Getter method to access formcontrols
  get service() {
    return this.pakagesUpdatedForm.get("service_id");
  }

  selectService(e) {
    var val = e.target.value.split(":")[1];
    this.service.setValue(val, {
      onlySelf: true,
    });
  }

  get f() {
    return this.pakagesUpdatedForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.pakagesUpdatedForm.invalid) {
      return;
    }

    const fd = new FormData();
    fd.append("id", this.pakagesID);
    fd.append("title", this.pakagesUpdatedForm.value.title);
    fd.append("start_date", this.pakagesUpdatedForm.value.start_date);
    fd.append("end_date", this.pakagesUpdatedForm.value.end_date);
    fd.append("service_id", this.pakagesUpdatedForm.value.service_id);
    fd.append("price", this.pakagesUpdatedForm.value.price);
    fd.append("image", this.pakagesUpdatedForm.get("image").value);

    this.pakagesService.updatePackages(fd).subscribe((data: any) => {
      console.log(data);
      if (data.code === 200) {
        this.toastr.success("Pakages Updated Successfully", "Success");
        this.submitted = false;
        this.router.navigate(["/packages"]);
      } else {
        this.toastr.error(data.message, "Error");
      }
    });
  }
}
