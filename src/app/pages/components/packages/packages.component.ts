import { Component, OnInit } from '@angular/core';
import { ToastrService } from "ngx-toastr";
import { PakagesService } from 'src/app/service/pakages.service';
import { environment } from './../../../../environments/environment';
import { remove } from 'lodash';


@Component({
  selector: 'app-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.scss']
})
export class PackagesComponent implements OnInit {


  base_image_url: string = environment.image_url;
  loadPakagesData: any;

  constructor(
    private pakagesService: PakagesService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.getAllPakages();
  }


  getAllPakages() {
    this.pakagesService.getPackages().subscribe((data: any) => {
      if (data.code === 200) {
        console.log(data)
        this.loadPakagesData = data.data;
      }
    });
  }


  deletePakages(innerCategory_id: any) {

    this.pakagesService.deletePackages({ id: innerCategory_id }).subscribe(
      (data: any) => {
        if (data.code === 200) {
          this.toastr.success("Pakages Deleted Successfully", "Success");

          remove(this.loadPakagesData, (innercategory: any) => {
            return innercategory.id === innerCategory_id
          });
          this.loadPakagesData = [...this.loadPakagesData];

        }
      },
      (error) => {
        this.toastr.error(error, "Error",);
      }
    );
  }

}
