
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { PakagesService } from "src/app/service/pakages.service";
import { ServiceService } from "src/app/service/service.service";

@Component({
  selector: 'app-add-package',
  templateUrl: './add-package.component.html',
  styleUrls: ['./add-package.component.scss']
})
export class AddPackageComponent implements OnInit {

  pakagesForm: FormGroup;
  loadServiceData: any = [];
  submitted = false;
  url = "";
  selectedFile: File = null;

  constructor(
    private formBuilder: FormBuilder,
    private pakagesService: PakagesService,
    private serviceService: ServiceService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.form();
    this.getAllServices();
  }

  form() {
    this.pakagesForm = this.formBuilder.group({
      title: ["", [Validators.required]],
      price: ["", [Validators.required]],
      service_id: ["", Validators.required],
      start_date: ["", Validators.required],
      end_date: ["", Validators.required],
      image: [""],
    });
  }


  getAllServices() {
    this.serviceService.getServices().subscribe((data: any) => {
      if (data.code === 200) {
        this.loadServiceData = data.data;
        console.log(this.loadServiceData)

      }
    });
  }

  // Getter method to access formcontrols
  get service() {
    return this.pakagesForm.get('service_id');
  }

  selectService(e) {
    var val = e.target.value.split(':')[1];
    this.service.setValue(val, {
      onlySelf: true
    })
  }

  // FOR IMAGE PREWVEW
  onSelectFile(e) {
    this.selectedFile = e.target.files[0];
    this.pakagesForm.get("image").setValue(this.selectedFile);

    if (e.target.files) {
      var reader = new FileReader();
      reader.readAsDataURL(this.selectedFile);
      reader.onload = (event: any) => {
        this.url = event.target.result;
      };
    }
  }

  get f() {
    return this.pakagesForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.pakagesForm.invalid) {
      return;
    }

    const fd = new FormData();
    fd.append("title", this.pakagesForm.value.title);
    fd.append("service_id", this.pakagesForm.value.service_id);
    fd.append("price", this.pakagesForm.value.price);
    fd.append("start_date", this.pakagesForm.value.start_date);
    fd.append("end_date", this.pakagesForm.value.end_date);
    fd.append("image", this.pakagesForm.get("image").value);

    this.pakagesService.addPackages(fd).subscribe((data: any) => {
      console.log(data);
      if (data.code === 200) {
        this.toastr.success("Pakages Added Successfully", "Success");
        this.pakagesForm.reset();
        this.url = ''
        this.submitted = false;
      } else {
        this.toastr.error(data.message, "Error");
      }
    });
  }
}
