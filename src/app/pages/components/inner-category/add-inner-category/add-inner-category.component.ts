
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { InnerCategoryService } from "src/app/service/inner-category.service";
import { ServiceService } from "src/app/service/service.service";
import { SlideCategoryService } from './../../../../service/slide-category.service';

@Component({
  selector: 'app-add-inner-category',
  templateUrl: './add-inner-category.component.html',
  styleUrls: ['./add-inner-category.component.scss']
})
export class AddInnerCategoryComponent implements OnInit {

  innerCategoryForm: FormGroup;
  loadServiceData: any = [];
  loadSlideCategoryData: any = [];
  submitted = false;
  url = "";
  selectedFile: File = null;

  constructor(
    private formBuilder: FormBuilder,
    private innerCategoryService: InnerCategoryService,
    private serviceService: ServiceService,
    private slideCategoryService: SlideCategoryService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.form();
    this.getAllServices();
    this.getAllSlideCategory();
  }

  form() {
    this.innerCategoryForm = this.formBuilder.group({
      title: ["", [Validators.required]],
      price: ["", [Validators.required]],
      service_id: ["", Validators.required],
      category_id: ["", Validators.required],
      image: [""],
    });
  }


  getAllServices() {
    this.serviceService.getServices().subscribe((data: any) => {
      if (data.code === 200) {
        this.loadServiceData = data.data;
        console.log(this.loadServiceData)

      }
    });
  }

  getAllSlideCategory() {
    this.slideCategoryService.getSlideCategory().subscribe((data: any) => {
      if (data.code === 200) {
        console.log(data)
        this.loadSlideCategoryData = data.data;
      }
    });
  }

  // Getter method to access formcontrols
  get service() {
    return this.innerCategoryForm.get('service_id');
  }

  selectService(e) {
    var val = e.target.value.split(':')[1];
    this.service.setValue(val, {
      onlySelf: true
    })
  }

  selectSlideCategory(e) {
    var val = e.target.value.split(':')[1];
    this.innerCategoryForm.get("category_id").setValue(val);
  }

  // FOR IMAGE PREWVEW
  onSelectFile(e) {
    this.selectedFile = e.target.files[0];
    this.innerCategoryForm.get("image").setValue(this.selectedFile);

    if (e.target.files) {
      var reader = new FileReader();
      reader.readAsDataURL(this.selectedFile);
      reader.onload = (event: any) => {
        this.url = event.target.result;
      };
    }
  }

  get f() {
    return this.innerCategoryForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.innerCategoryForm.invalid) {
      return;
    }

    const fd = new FormData();
    fd.append("title", this.innerCategoryForm.value.title);
    fd.append("category_id", this.innerCategoryForm.value.category_id);
    fd.append("service_id", this.innerCategoryForm.value.service_id);
    fd.append("price", this.innerCategoryForm.value.price);
    fd.append("image", this.innerCategoryForm.get("image").value);

    this.innerCategoryService.addInnerCategory(fd).subscribe((data: any) => {
      console.log(data);
      if (data.code === 200) {
        this.toastr.success("Service Added Successfully", "Success");
        this.innerCategoryForm.reset();
        this.url = ''
        this.submitted = false;
      } else {
        this.toastr.error(data.message, "Error");
      }
    });
  }
}
