import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddInnerCategoryComponent } from './add-inner-category.component';

describe('AddInnerCategoryComponent', () => {
  let component: AddInnerCategoryComponent;
  let fixture: ComponentFixture<AddInnerCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddInnerCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddInnerCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
