import { Component, OnInit } from '@angular/core';
import { ToastrService } from "ngx-toastr";
import { InnerCategoryService } from 'src/app/service/inner-category.service';
import { environment } from './../../../../environments/environment';
import { remove } from 'lodash';

@Component({
  selector: 'app-inner-category',
  templateUrl: './inner-category.component.html',
  styleUrls: ['./inner-category.component.scss']
})
export class InnerCategoryComponent implements OnInit {

  base_image_url: string = environment.image_url;
  loadInnerCategoryData: any;

  constructor(
    private innerCategoryService: InnerCategoryService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.getAllInnerCategory();
  }


  getAllInnerCategory() {
    this.innerCategoryService.getInnerCategory().subscribe((data: any) => {
      if (data.code === 200) {
        console.log(data)
        this.loadInnerCategoryData = data.data;
      }
    });
  }


  deleteInnerCaegory(innerCategory_id: any) {

    this.innerCategoryService.deleteInnerCategory({ id: innerCategory_id }).subscribe(
      (data: any) => {
        if (data.code === 200) {
          this.toastr.success("Service Deleted Successfully", "Success");

          remove(this.loadInnerCategoryData, (innercategory: any) => {
            return innercategory.id === innerCategory_id
          });
          this.loadInnerCategoryData = [...this.loadInnerCategoryData];

        }
      },
      (error) => {
        this.toastr.error(error, "Error",);
      }
    );
  }

}
