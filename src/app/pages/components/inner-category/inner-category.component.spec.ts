import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InnerCategoryComponent } from './inner-category.component';

describe('InnerCategoryComponent', () => {
  let component: InnerCategoryComponent;
  let fixture: ComponentFixture<InnerCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InnerCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InnerCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
