import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { InnerCategoryService } from "src/app/service/inner-category.service";
import { ServiceService } from "src/app/service/service.service";
import { SlideCategoryService } from "./../../../../service/slide-category.service";
import { environment } from "./../../../../../environments/environment";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-edit-inner-category",
  templateUrl: "./edit-inner-category.component.html",
  styleUrls: ["./edit-inner-category.component.scss"],
})
export class EditInnerCategoryComponent implements OnInit {
  base_image_url: string = environment.image_url;
  innerCategoryUpdatedForm: FormGroup;
  submitted = false;
  loadServiceData: any = [];
  loadSlideCategoryData: any = [];
  loadInnerCategoryData: any;
  selectedFile: File = null;
  innerCategoryID: any;
  loadInnerCategoryImage: any;
  Hours: any = [12, 24, 48, 78];

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private innerCategoryService: InnerCategoryService,
    private serviceService: ServiceService,
    private slideCategoryService: SlideCategoryService,
    private toastr: ToastrService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.form();
    this.innerCategoryID = this.activatedRoute.snapshot.paramMap.get("id");
    this.editService();
    this.getAllServices();
    this.getAllSlideCategory();
  }

  form() {
    this.innerCategoryUpdatedForm = this.formBuilder.group({
      title: ["", [Validators.required]],
      price: ["", [Validators.required]],
      service_id: ["", Validators.required],
      category_id: ["", Validators.required],
      image: [""],
    });
  }

  getAllServices() {
    this.serviceService.getServices().subscribe((data: any) => {
      if (data.code === 200) {
        this.loadServiceData = data.data;
        console.log(this.loadServiceData);
      }
    });
  }

  getAllSlideCategory() {
    this.slideCategoryService.getSlideCategory().subscribe((data: any) => {
      if (data.code === 200) {
        console.log(data);
        this.loadSlideCategoryData = data.data;
      }
    });
  }

  editService() {
    this.innerCategoryService
      .editInnerCategory({ id: this.innerCategoryID })
      .subscribe(
        (data: any) => {
          console.log(data);
          if (data.code === 200) {
            this.loadInnerCategoryData = data.data;
            this.loadInnerCategoryImage =
              this.base_image_url + this.loadInnerCategoryData.image;
            console.log(this.loadInnerCategoryData);
          }
        },
        (error) => {
          this.toastr.error(error, "Error");
        }
      );
  }

  // FOR IMAGE PREWVEW
  onSelectFile(e) {
    this.selectedFile = e.target.files[0];
    this.innerCategoryUpdatedForm.get("image").setValue(this.selectedFile);

    if (e.target.files) {
      var reader = new FileReader();
      reader.readAsDataURL(this.selectedFile);
      reader.onload = (event: any) => {
        this.loadInnerCategoryImage = event.target.result;
      };
    }
  }

  // Getter method to access formcontrols
  get service() {
    return this.innerCategoryUpdatedForm.get("service_id");
  }

  selectService(e) {
    var val = e.target.value.split(":")[1];
    this.service.setValue(val, {
      onlySelf: true,
    });
  }

  selectSlideCategory(e) {
    var val = e.target.value.split(":")[1];
    this.innerCategoryUpdatedForm.get("category_id").setValue(val);
  }

  get f() {
    return this.innerCategoryUpdatedForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.innerCategoryUpdatedForm.invalid) {
      return;
    }

    const fd = new FormData();
    fd.append("id", this.innerCategoryID);
    fd.append("title", this.innerCategoryUpdatedForm.value.title);
    fd.append("category_id", this.innerCategoryUpdatedForm.value.category_id);
    fd.append("service_id", this.innerCategoryUpdatedForm.value.service_id);
    fd.append("price", this.innerCategoryUpdatedForm.value.price);
    fd.append("image", this.innerCategoryUpdatedForm.get("image").value);

    this.innerCategoryService.updaInnerCategory(fd).subscribe((data: any) => {
      console.log(data);
      if (data.code === 200) {
        this.toastr.success("Inner Category Updated Successfully", "Success");
        this.submitted = false;
        this.router.navigate(["/inner-category"]);
      } else {
        this.toastr.error(data.message, "Error");
      }
    });
  }
}
