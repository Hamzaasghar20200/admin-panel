import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditInnerCategoryComponent } from './edit-inner-category.component';

describe('EditInnerCategoryComponent', () => {
  let component: EditInnerCategoryComponent;
  let fixture: ComponentFixture<EditInnerCategoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditInnerCategoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditInnerCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
