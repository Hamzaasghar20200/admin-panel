import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  key: string = 'name';
  reverse: boolean = false;
  filter: boolean;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }
  p: number = 1;
  title = 'app works!';
  games = [
    {
      "id":"1",
      "number": 1324,
      "status": "Strategy",
      "detail": "pending",
      "amount": 200
    },
    {
      "id":"2",
      "number": 1321,
      "status": "Strategy",
      "detail": "confirmed",
      "amount": 200
    },
    {
      "id":"3",
      "number": 1321,
      "status": "Strategy",
      "detail": "confirmed",
      "amount": 200
    },
    {
      "id":"4",
      "number": 1321,
      "status": "Strategy",
      "detail": "confirmed",
      "amount": 200
    },
    {
      "id":"5",
      "number": 1321,
      "status": "Strategy",
      "detail": "confirmed",
      "amount": 200
    },
    {
      "id":"6",
      "number": 1321,
      "status": "Strategy",
      "detail": "confirmed",
      "amount": 200
    },
    {
      "id":"7",
      "number": 1321,
      "status": "Strategy",
      "detail": "confirmed",
      "amount": 200
    },
    {
      "id":"8",
      "number": 1321,
      "status": "Strategy",
      "detail": "confirmed",
      "amount": 200
    },
    {
      "id":"9",
      "number": 1321,
      "status": "Strategy",
      "detail": "confirmed",
      "amount": 200
    },
    {
      "id":"10",
      "number": 1321,
      "status": "Strategy",
      "detail": "confirmed",
      "amount": 200
    },
    {
      "id":"11",
      "number": 1321,
      "status": "pending",
      "detail": "confirmed",
      "amount": 200
    },
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
