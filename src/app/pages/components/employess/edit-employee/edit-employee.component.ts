import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { environment } from "./../../../../../environments/environment";
import { ActivatedRoute, Router } from "@angular/router";
import { EmployeeService } from "src/app/service/employee.service";

@Component({
  selector: "app-edit-employee",
  templateUrl: "./edit-employee.component.html",
  styleUrls: ["./edit-employee.component.scss"],
})
export class EditEmployeeComponent implements OnInit {
  base_image_url: string = environment.image_url;
  loadEmployee: any;
  selectedFile: File = null;
  employeeID: any;
  loadEmployeImage: any;
  employeeUpdate: FormGroup;
  submitted= false;

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private employeeService: EmployeeService,
    private toastr: ToastrService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.form();
    this.employeeID = this.activatedRoute.snapshot.paramMap.get("id");
    this.editEmployee();
  }

  editEmployee() {
    this.employeeService.editEmploye({ id: this.employeeID }).subscribe(
      (data: any) => {
        console.log(data);
        if (data.code === 200) {
          this.loadEmployee = data.data;
          this.loadEmployeImage = this.base_image_url + this.loadEmployee.user_image;
          console.log(this.loadEmployeImage)
        }
      },
      (error) => {
        this.toastr.error(error, "Error");
      }
    );
  }

  // FOR IMAGE PREWVEW
  onSelectFile(e) {
    this.selectedFile = e.target.files[0];
    this.employeeUpdate.get("image").setValue(this.selectedFile);

    if (e.target.files) {
      var reader = new FileReader();
      reader.readAsDataURL(this.selectedFile);
      reader.onload = (event: any) => {
        this.loadEmployeImage = event.target.result;
      };
    }
  }

  form() {
    this.employeeUpdate = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      name: ["", [Validators.required]],
      phone_number: ["", Validators.required],
      address: ["", Validators.required],
      image: [""],
    });
  }

  get f() {
    return this.employeeUpdate.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.employeeUpdate.invalid) {
      return;
    }

    const fd = new FormData();
    fd.append("name", this.employeeUpdate.value.name);
    fd.append("email", this.employeeUpdate.value.email);
    fd.append("phone_number", this.employeeUpdate.value.phone_number);
    fd.append("address", this.employeeUpdate.value.address);
    fd.append("image", this.employeeUpdate.get("image").value);

    this.employeeService.editEmploye(fd).subscribe((data: any) => {
      console.log(data);
      if (data.code === 200) {
        this.toastr.success("Employee Updated Successfully", "Success");
        this.submitted = false;
        this.router.navigate(['/employess']);
      } else {
        this.toastr.error(data.message, "Error");
      }
    });

  }
}
