import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { EmployeeService } from "src/app/service/employee.service";

@Component({
  selector: "app-add-employee",
  templateUrl: "./add-employee.component.html",
  styleUrls: ["./add-employee.component.scss"],
})
export class AddEmployeeComponent implements OnInit {
  employeeForm: FormGroup;
  submitted = false;
  url = "";
  selectedFile: File = null;

  constructor(
    private formBuilder: FormBuilder,
    private employeeService: EmployeeService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.form();
  }

  form() {
    this.employeeForm = this.formBuilder.group({
      email: ["", [Validators.required, Validators.email]],
      name: ["", [Validators.required]],
      phone_number: ["", Validators.required],
      address: ["", Validators.required],
      image: [""],
    });
  }

  // FOR IMAGE PREWVEW
  onSelectFile(e) {
    this.selectedFile = e.target.files[0];
    this.employeeForm.get("image").setValue(this.selectedFile);

    if (e.target.files) {
      var reader = new FileReader();
      reader.readAsDataURL(this.selectedFile);
      reader.onload = (event: any) => {
        this.url = event.target.result;
      };
    }
  }

  get f() {
    return this.employeeForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.employeeForm.invalid) {
      return;
    }


    const fd = new FormData();
    fd.append("name", this.employeeForm.value.name);
    fd.append("email", this.employeeForm.value.email);
    fd.append("phone_number", this.employeeForm.value.phone_number);
    fd.append("address", this.employeeForm.value.address);
    fd.append("image", this.employeeForm.get("image").value);

    this.employeeService.addEmploye(fd).subscribe((data: any) => {
      console.log(data);
      if (data.code === 200) {
        this.toastr.success("Employee Added Successfully", "Success");
        this.employeeForm.reset();
        this.url = ''
        this.submitted = false;
      } else {
        this.toastr.error(data.message, "Error");
      }
    });
  }
}
