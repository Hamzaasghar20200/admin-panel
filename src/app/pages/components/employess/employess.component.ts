import { Component, OnInit } from '@angular/core';
import { ToastrService } from "ngx-toastr";
import { EmployeeService } from 'src/app/service/employee.service';
import { environment } from './../../../../environments/environment';
import { remove } from 'lodash';

@Component({
  selector: 'app-employess',
  templateUrl: './employess.component.html',
  styleUrls: ['./employess.component.scss']
})
export class EmployessComponent implements OnInit {

  base_image_url: string = environment.image_url;
  loadEmployeeData: any;

  constructor(
    private employeeService: EmployeeService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.getAllEmployees();
  }

  getAllEmployees() {
    this.employeeService.getEmployees({}).subscribe((data: any) => {
      if (data.code === 200) {
        console.log(data)
        this.loadEmployeeData = data.data;
      }
    });
  }


  deleteEmploye(employee_id: any) {

    this.employeeService.deleteEmploye({ id: employee_id }).subscribe(
      (data: any) => {
        if (data.code === 200) {
          this.toastr.success("User Deleted Successfully", "Success");

          remove(this.loadEmployeeData, (employee: any) => {
            return employee.id === employee_id
          });
          this.loadEmployeeData = [...this.loadEmployeeData];

        }
      },
      (error) => {
        this.toastr.error(error, "Error",);
      }
    );
  }

}
