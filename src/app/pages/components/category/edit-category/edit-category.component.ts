import { LocalService } from "./../../../../service/local.service";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { environment } from "./../../../../../environments/environment";
import { ActivatedRoute, Router } from "@angular/router";
import { ServiceService } from "src/app/service/service.service";

@Component({
  selector: "app-edit-category",
  templateUrl: "./edit-category.component.html",
  styleUrls: ["./edit-category.component.scss"],
})
export class EditCategoryComponent implements OnInit {
  base_image_url: string = environment.image_url;
  serviceUpdatedForm: FormGroup;
  submitted = false;
  loadService: any;
  selectedFile: File = null;
  serviceID: any;
  loadServiceImage: any;
  Hours: any = [12, 24, 48, 78];

  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private serviceService: ServiceService,
    private toastr: ToastrService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.form();
    this.serviceID = this.activatedRoute.snapshot.paramMap.get("id");
    this.editService();
  }

  form() {
    this.serviceUpdatedForm = this.formBuilder.group({
      title: ["", [Validators.required]],
      price: ["", [Validators.required]],
      min_time: ["", Validators.required],
      image: [""],
    });
  }

  editService() {
    this.serviceService.editService({ id: this.serviceID }).subscribe(
      (data: any) => {
        console.log(data);
        if (data.code === 200) {
          this.loadService = data.data;
          this.loadServiceImage =
            this.base_image_url + this.loadService.image;
          console.log(this.loadService);
        }
      },
      (error) => {
        this.toastr.error(error, "Error");
      }
    );
  }

  // FOR IMAGE PREWVEW
  onSelectFile(e) {
    this.selectedFile = e.target.files[0];
    this.serviceUpdatedForm.get("image").setValue(this.selectedFile);

    if (e.target.files) {
      var reader = new FileReader();
      reader.readAsDataURL(this.selectedFile);
      reader.onload = (event: any) => {
        this.loadServiceImage = event.target.result;
      };
    }
  }

  // Getter method to access formcontrols
  get minTime() {
    return this.serviceUpdatedForm.get('min_time');
  }

  changeHours(e) {
    var val = e.target.value.split(':')[1];
    this.minTime.setValue(val, {
      onlySelf: true
    })
    // this.serviceForm.get("min_time").setValue(e.target.value);
  }

  get f() {
    return this.serviceUpdatedForm.controls;
  }


  onSubmit() {
    this.submitted = true;

    if (this.serviceUpdatedForm.invalid) {
      return;
    }

    const fd = new FormData();
    fd.append("id", this.serviceID);
    fd.append("title", this.serviceUpdatedForm.value.title);
    fd.append("min_time", this.serviceUpdatedForm.value.min_time);
    fd.append("price", this.serviceUpdatedForm.value.price);
    fd.append("image", this.serviceUpdatedForm.get("image").value);

    this.serviceService.updateService(fd).subscribe((data: any) => {
      console.log(data);
      if (data.code === 200) {
        this.toastr.success("Service Updated Successfully", "Success");
        this.submitted = false;
        this.router.navigate(["/category"]);
      } else {
        this.toastr.error(data.message, "Error");
      }
    });
  }
}
