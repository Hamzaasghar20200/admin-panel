import { Component, OnInit } from '@angular/core';
import { ToastrService } from "ngx-toastr";
import { ServiceService } from 'src/app/service/service.service';
import { environment } from './../../../../environments/environment';
import { remove } from 'lodash';


@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  base_image_url: string = environment.image_url;
  loadServiceData: any;

  constructor(
    private serviceService: ServiceService,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.getAllServices();
  }


  getAllServices() {
    this.serviceService.getServices().subscribe((data: any) => {
      if (data.code === 200) {
        console.log(data)
        this.loadServiceData = data.data;
      }
    });
  }


  deleteService(service_id: any) {

    this.serviceService.deleteService({ id: service_id }).subscribe(
      (data: any) => {
        if (data.code === 200) {
          this.toastr.success("Service Deleted Successfully", "Success");

          remove(this.loadServiceData, (service: any) => {
            return service.id === service_id
          });
          this.loadServiceData = [...this.loadServiceData];

        }
      },
      (error) => {
        this.toastr.error(error, "Error",);
      }
    );
  }

}
