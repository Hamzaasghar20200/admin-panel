import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ToastrService } from "ngx-toastr";
import { ServiceService } from "src/app/service/service.service";

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnInit {

  serviceForm: FormGroup;
  submitted = false;
  url = "";
  selectedFile: File = null;
  Hours: any = [12, 24, 48, 78]

  constructor(
    private formBuilder: FormBuilder,
    private serviceService: ServiceService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    this.form();
  }

  form() {
    this.serviceForm = this.formBuilder.group({
      title: ["", [Validators.required]],
      price: ["", [Validators.required]],
      min_time: ["", Validators.required],
      image: [""],
    });
  }

  // Getter method to access formcontrols
  get minTime() {
    return this.serviceForm.get('min_time');
  }

  changeHours(e) {
    var val = e.target.value.split(':')[1];
    this.minTime.setValue(val, {
      onlySelf: true
    })
    // this.serviceForm.get("min_time").setValue(e.target.value);
  }

  // FOR IMAGE PREWVEW
  onSelectFile(e) {
    this.selectedFile = e.target.files[0];
    this.serviceForm.get("image").setValue(this.selectedFile);

    if (e.target.files) {
      var reader = new FileReader();
      reader.readAsDataURL(this.selectedFile);
      reader.onload = (event: any) => {
        this.url = event.target.result;
      };
    }
  }

  get f() {
    return this.serviceForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    if (this.serviceForm.invalid) {
      return;
    }

    const fd = new FormData();
    fd.append("title", this.serviceForm.value.title);
    fd.append("min_time", this.serviceForm.value.min_time);
    fd.append("price", this.serviceForm.value.price);
    fd.append("image", this.serviceForm.get("image").value);

    this.serviceService.addService(fd).subscribe((data: any) => {
      console.log(data);
      if (data.code === 200) {
        this.toastr.success("Service Added Successfully", "Success");
        this.serviceForm.reset();
        this.url = ''
        this.submitted = false;
      } else {
        this.toastr.error(data.message, "Error");
      }
    });
  }
}
