import { Routes } from "@angular/router";

import { DashboardComponent } from "../../pages/dashboard/dashboard.component";
import { AboutComponent } from "../../pages/components/about/about.component";
import { CategoryComponent } from "../../pages/components/category/category.component";
import { EmployessComponent } from "../../pages/components/employess/employess.component";
import { InnerCategoryComponent } from "../../pages/components/inner-category/inner-category.component";
import { NotificationComponent } from "../../pages/components/notification/notification.component";
import { OrdersComponent } from "../../pages/components/orders/orders.component";
import { PackagesComponent } from "../../pages/components/packages/packages.component";
import { SalesComponent } from "../../pages/components/sales/sales.component";
import { ServicesComponent } from "../../pages/components/services/services.component";
import { SliderComponent } from "../../pages/components/slider/slider.component";
import { UsersComponent } from "../../pages/components/users/users.component";
import { LoginComponent } from 'src/app/login/login.component';
import { AuthGuardService } from './../../service/auth-guard.service';
import { OrderViewPageComponent } from '../../pages/components/order-view-page/order-view-page.component';
import { AddCategoryComponent } from '../../pages/components/category/add-category/add-category.component';
import { EditCategoryComponent } from '../../pages/components/category/edit-category/edit-category.component';
import { AddInnerCategoryComponent } from '../../pages/components/inner-category/add-inner-category/add-inner-category.component';
import { EditInnerCategoryComponent } from '../../pages/components/inner-category/edit-inner-category/edit-inner-category.component';
import { SlideCategoryComponent } from '../../pages/components/slide-category/slide-category.component';
import { SlideAddCategoryComponent } from '../../pages/components/slide-category/slide-add-category/slide-add-category.component';
import { SlideEditCategoryComponent } from '../../pages/components/slide-category/slide-edit-category/slide-edit-category.component';
// import { RtlComponent } from "../../pages/rtl/rtl.component";
import { AddSliderComponent } from '../../pages/components/slider/add-slider/add-slider.component';
import { EditSliderComponent } from '../../pages/components/slider/edit-slider/edit-slider.component';
import { AddEmployeeComponent } from '../../pages/components/employess/add-employee/add-employee.component';
import { EditEmployeeComponent } from '../../pages/components/employess/edit-employee/edit-employee.component';
import { AddPackageComponent } from '../../pages/components/packages/add-package/add-package.component';
import { EditPackageComponent } from '../../pages/components/packages/edit-package/edit-package.component';



export const AdminLayoutRoutes: Routes = [
  // { path: "", component: LoginComponent },
  { path: "dashboard", component: DashboardComponent, canActivate: [AuthGuardService] },
  { path: "abouts", component: AboutComponent, canActivate: [AuthGuardService] },
  { path: "users", component: UsersComponent, canActivate: [AuthGuardService] },

  { path: "add-employess", component: AddEmployeeComponent, canActivate: [AuthGuardService] },
  { path: "edit-employess/:id", component: EditEmployeeComponent, canActivate: [AuthGuardService] },

  { path: "add-package", component: AddPackageComponent, canActivate: [AuthGuardService] },
  { path: "edit-package/:id", component: EditPackageComponent, canActivate: [AuthGuardService] },

  { path: "slider", component: SliderComponent, canActivate: [AuthGuardService] },
  { path: "services", component: ServicesComponent, canActivate: [AuthGuardService] },

  { path: "edit-category/:id", component: EditCategoryComponent, canActivate: [AuthGuardService] },
  { path: "add-category", component: AddCategoryComponent, canActivate: [AuthGuardService] },

  { path: "category-slide", component: SlideCategoryComponent, canActivate: [AuthGuardService] },
  { path: "add-slide", component: SlideAddCategoryComponent, canActivate: [AuthGuardService] },
  { path: "edit-slide/:id", component: SlideEditCategoryComponent, canActivate: [AuthGuardService] },

  { path: "add-slider", component: AddSliderComponent, canActivate: [AuthGuardService] },
  { path: "edit-slider/:id", component: EditSliderComponent, canActivate: [AuthGuardService] },

  { path: "add-inner", component: AddInnerCategoryComponent, canActivate: [AuthGuardService] },
  { path: "edit-inner/:id", component: EditInnerCategoryComponent, canActivate: [AuthGuardService] },
  { path: "sales", component: SalesComponent, canActivate: [AuthGuardService] },
  { path: "packages", component: PackagesComponent, canActivate: [AuthGuardService] },
  { path: "order-view", component: OrderViewPageComponent, canActivate: [AuthGuardService] },
  { path: "orders", component: OrdersComponent, canActivate: [AuthGuardService] },
  { path: "notifications", component: NotificationComponent, canActivate: [AuthGuardService] },
  { path: "inner-category", component: InnerCategoryComponent, canActivate: [AuthGuardService]  },
  { path: "category", component: CategoryComponent, canActivate: [AuthGuardService]  },
  { path: "employess", component: EmployessComponent, canActivate: [AuthGuardService] },
];
