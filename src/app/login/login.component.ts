import { User } from "./../model/user";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { AuthService } from "./../service/auth.service";
import { LocalService } from "./../service/local.service";
import { ToastrService } from "ngx-toastr";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit {
  authForm: FormGroup;
  loading: boolean = false;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private toastr: ToastrService,
    private authService: AuthService,
    private localService: LocalService
  ) {
    if (this.localService.getUserPayload()) {
      router.navigate(["/dashboard"]);
    }
  }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm() {
    this.authForm = this.fb.group({
      email: ["", Validators.required],
      password: ["", Validators.required],
    });
  }

  onSubmit() {
    if (this.authForm.invalid) {
      return;
    }

    this.loading = true;

    let { email, password } = this.authForm.value;
    let user: any = { email, password };
    this.authService.login(user).subscribe(
      (data: any) => {
        if (data.code === 200) {
          this.localService.setUserPayload(data.data);
          this.router.navigate(["/dashboard"]);
        } else {
          this.toastr.error(data.message, "Error", {
            positionClass: "toast-top-center",
          });
          this.loading = false;
        }
      },
      (error) => {
        this.toastr.error(error, "", {
          positionClass: "toast-top-center",
        });
        this.loading = false;
      }
    );
  }
}
