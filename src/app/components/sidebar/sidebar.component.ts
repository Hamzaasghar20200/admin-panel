import { Component, OnInit } from "@angular/core";

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  {
    path: "/dashboard",
    title: "Dashboard",
    icon: "icon-chart-pie-36",
    class: ""
  },
  {
    path: "/users",
    title: "Manage Users",
    icon: "icon-chart-pie-36",
    class: ""
  },
  {
    path: "/orders",
    title: "Manage Orders",
    icon: "icon-atom",
    class: ""
  },
  {
    path: "/category",
    title: "Manage  Category",
    icon: "icon-align-center",
    class: ""
  },
  {
    path: "/category-slide",
    title: "Manage  Category Slide",
    icon: "icon-align-center",
    class: ""
  },
  {
    path: "/inner-category",
    title: "Manage Inner Category",
    icon: "icon-align-center",
    class: ""
  },
  {
    path: "/slider",
    title: "Manage Home  Slider",
    icon: "icon-world",
    class: ""
  },
  {
    path: "/employess",
    title: "Manage Employess",
    icon: "icon-puzzle-10",
    class: ""
  },
  {
    path: "/packages",
    title: "Manage Packages",
    icon: "icon-pin",
    class: "" 
  },
  // {
  //   path: "/services",
  //   title: "Manage Services",
  //   icon: "icon-bell-55",
  //   class: ""
  // },

  {
    path: "/sales",
    title: "Manage  Sale",
    icon: "icon-single-02",
    class: ""
  },
 
 
  {
    path: "/notifications",
    title: "Manage Notification",
    icon: "icon-world",
    class: ""
  },
  {
    path: "/abouts",
    title: "Manage About",
    icon: "icon-world",
    class: ""
  },
 
];

@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.css"]
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor() {}

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
    if (window.innerWidth > 991) {
      return false;
    }
    return true;
  }
}
