export class User {
  id: string;
  email: string;
  name: string;
  password: string;
  phone_number: string;
  firebase_token: string;
  user_image: string;
  roles: string;
  status: string;
  created_at: string;
  updated_at: string;
}
