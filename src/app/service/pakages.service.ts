import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PakagesService {

  constructor(
    private httpClient: HttpClient
  ) { }


  getPackages(): Observable<any> {
    return this.httpClient.get<any>(`${environment.api_url}/manage_packages`)
  }

  addPackages(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/add_package`, body)
  }

  editPackages(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/edit_package`, body)
  }

  updatePackages(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/update_package`, body)
  }

  deletePackages(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/delete_package`, body)
  }

}
