import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class InnerCategoryService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getInnerCategory(): Observable<any> {
    return this.httpClient.get<any>(`${environment.api_url}/manage_products`)
  }

  addInnerCategory(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/add_product	`, body)
  }

  editInnerCategory(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/edit_product`, body)
  }

  updaInnerCategory(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/update_product`, body)
  }

  deleteInnerCategory(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/delete_product`, body)
  }
}
