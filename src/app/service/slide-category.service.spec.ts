import { TestBed } from '@angular/core/testing';

import { SlideCategoryService } from './slide-category.service';

describe('SlideCategoryService', () => {
  let service: SlideCategoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SlideCategoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
