import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(
    private httpClient: HttpClient
  ) { }


  getEmployees(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/manage_employees`, body)
  }

  addEmploye(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/add_employee`, body)
  }

  editEmploye(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/edit_employee`, body)
  }

  updateEmploye(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/update_employee`, body)
  }

  deleteEmploye(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/delete_employee`, body)
  }

}
