import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SliderService {

  constructor(
    private httpClient: HttpClient
  ) { }


  getSlider(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/manage_slider`, body)
  }

  addSlider(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/add_banner`, body)
  }

  editSlider(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/edit_slider`, body)
  }

  updateSlider(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/update_slider`, body)
  }

  deleteSlider(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/delete_slider`, body)
  }

}
