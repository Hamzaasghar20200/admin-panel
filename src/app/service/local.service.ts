import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { User } from './../model/user';

@Injectable({
  providedIn: 'root'
})
export class LocalService {

  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;

  constructor() {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
      return this.currentUserSubject.value;
  }


  setUserPayload(user: any){
    window.localStorage.setItem('user_payload', JSON.stringify(user));
    this.currentUserSubject.next(user);
  }
  getUserPayload(){
    return  window.localStorage.getItem('user_payload');
  }
  destroyUserPayload(){
    window.localStorage.removeItem('user_payload');
  }

}
