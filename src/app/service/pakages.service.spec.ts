import { TestBed } from '@angular/core/testing';

import { PakagesService } from './pakages.service';

describe('PakagesService', () => {
  let service: PakagesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PakagesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
