import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from './../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private httpClient: HttpClient
  ) { }


  listOfUsers(): Observable<User> {
    return this.httpClient.get<User>(`${environment.api_url}/list_of_users`)
  }

  blockUser(body: any): Observable<User> {
    return this.httpClient.post<User>(`${environment.api_url}/block_user`, body)
  }

  unBlockUser(body: any): Observable<User> {
    return this.httpClient.post<User>(`${environment.api_url}/un_block_user`, body)
  }

  delete_user(body: any): Observable<User> {
    return this.httpClient.post<User>(`${environment.api_url}/delete_user`, body)
  }

}
