import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SlideCategoryService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getSlideCategory(): Observable<any> {
    return this.httpClient.get<any>(`${environment.api_url}/manage_categories`)
  }

  addSlideCategory(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/add_category	`, body)
  }

  editSlideCategory(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/edit_category`, body)
  }

  updateSlideCategory(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/update_category`, body)
  }

  deleteSlideCategory(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/delete_category`, body)
  }
}
