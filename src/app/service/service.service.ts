import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(
    private httpClient: HttpClient
  ) { }


  getServices(): Observable<any> {
    return this.httpClient.get<any>(`${environment.api_url}/manage_services`)
  }

  addService(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/add_service`, body)
  }

  editService(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/edit_service	`, body)
  }

  updateService(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/update_service	`, body)
  }

  deleteService(body: any): Observable<any> {
    return this.httpClient.post<any>(`${environment.api_url}/delete_service`, body)
  }

}
