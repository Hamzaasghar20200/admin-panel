import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";
import { RouterModule } from "@angular/router";
import { ToastrModule } from 'ngx-toastr';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AppComponent } from "./app.component";
import { AdminLayoutComponent } from "./layouts/admin-layout/admin-layout.component";
import { Ng2OrderModule } from 'ng2-order-pipe';
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import {NgxPaginationModule} from 'ngx-pagination';


import { AppRoutingModule } from "./app-routing.module";
import { ComponentsModule } from "./components/components.module";
import { LoginComponent } from './login/login.component';
import { OrdersComponent } from './pages/components/orders/orders.component';
import { PackagesComponent } from './pages/components/packages/packages.component';
import { ServicesComponent } from './pages/components/services/services.component';
import { SalesComponent } from './pages/components/sales/sales.component';
import { EmployessComponent } from './pages/components/employess/employess.component';
import { CategoryComponent } from './pages/components/category/category.component';
import { InnerCategoryComponent } from './pages/components/inner-category/inner-category.component';
import { UsersComponent } from './pages/components/users/users.component';
import { NotificationComponent } from './pages/components/notification/notification.component';
import { AboutComponent } from './pages/components/about/about.component';
import { SliderComponent } from './pages/components/slider/slider.component';
import { OrderViewPageComponent } from './pages/components/order-view-page/order-view-page.component';
import { AddCategoryComponent } from './pages/components/category/add-category/add-category.component';
import { EditCategoryComponent } from './pages/components/category/edit-category/edit-category.component';
import { EditInnerCategoryComponent } from './pages/components/inner-category/edit-inner-category/edit-inner-category.component';
import { AddInnerCategoryComponent } from './pages/components/inner-category/add-inner-category/add-inner-category.component';
import { SlideCategoryComponent } from './pages/components/slide-category/slide-category.component';
import { SlideAddCategoryComponent } from './pages/components/slide-category/slide-add-category/slide-add-category.component';
import { SlideEditCategoryComponent } from './pages/components/slide-category/slide-edit-category/slide-edit-category.component';
import { AddSliderComponent } from './pages/components/slider/add-slider/add-slider.component';
import { EditSliderComponent } from './pages/components/slider/edit-slider/edit-slider.component';
import { AddEmployeeComponent } from './pages/components/employess/add-employee/add-employee.component';
import { EditEmployeeComponent } from './pages/components/employess/edit-employee/edit-employee.component';
import { AddPackageComponent } from './pages/components/packages/add-package/add-package.component';
import { EditPackageComponent } from './pages/components/packages/edit-package/edit-package.component';



@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ComponentsModule,
    NgbModule,
    RouterModule,
    AppRoutingModule,
    Ng2SearchPipeModule,
    Ng2OrderModule,
    NgxPaginationModule,
    ToastrModule.forRoot({
      timeOut: 5000,
      positionClass: 'toast-top-center'
    })
  ],
  declarations: [AppComponent, AdminLayoutComponent, LoginComponent, OrdersComponent, PackagesComponent, ServicesComponent, SalesComponent, EmployessComponent, CategoryComponent, InnerCategoryComponent, UsersComponent, NotificationComponent, AboutComponent, SliderComponent, OrderViewPageComponent, AddCategoryComponent, EditCategoryComponent, EditInnerCategoryComponent, AddInnerCategoryComponent, SlideCategoryComponent, SlideAddCategoryComponent, SlideEditCategoryComponent, AddSliderComponent, EditSliderComponent, AddEmployeeComponent, EditEmployeeComponent, AddPackageComponent, EditPackageComponent],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
